import mlflow
import numpy as np

class SklearnModelWrapper(mlflow.pyfunc.PythonModel):
    def __init__(self, model, threshold_0_1, threshold_1_2):
        self.model = model
        self.threshold_0_1 = threshold_0_1
        self.threshold_1_2 = threshold_1_2

    def predict(self, context, model_input):
        x_arr = np.array(model_input)
        predictions = self.model.predict(x_arr.reshape(1, -1))
        output = {
            "class": predictions,
            "threshold_0_1": self.threshold_0_1,
            "threshold_1_2": self.threshold_1_2
        }
        if predictions < self.threshold_0_1:
            output['class_str'] = 'setosa'
            return output
        elif predictions < self.threshold_1_2:
            output['class_str'] = 'versicolor'
            return output
        else:
            output['class_str'] = 'virginica'
            return output


