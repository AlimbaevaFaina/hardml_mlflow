from flask import Flask, jsonify, request
import numpy as np
import models

FLASK_HOST = '0.0.0.0'
FLASK_PORT = '2000'

app = Flask(__name__)

@app.route("/get_source_iris_pred")
def get_source_iris_pred():
    sepal_length = request.args.get("sepal_length")
    sepal_width = request.args.get("sepal_width")
    X = np.array([[sepal_length, sepal_width]])
    prediction = models.model_float.predict(X)
    return jsonify({'prediction': prediction[0]})

@app.route("/get_string_iris_pred")
def get_string_iris_pred():
    sepal_length = request.args.get("sepal_length")
    sepal_width = request.args.get("sepal_width")
    prediction = models.model_string.predict([sepal_length, sepal_width])
    return jsonify(prediction)

# if __name__ == "__main__":
app.run(host=FLASK_HOST, port=FLASK_PORT)




