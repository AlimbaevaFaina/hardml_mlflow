FROM python:3.9

WORKDIR /app
COPY ./service_2 /app
# COPY ./service_1 /app
COPY . /app

RUN pip install flask mlflow==2.1.1 boto3==1.20.19 scikit-learn==1.2.2

ENTRYPOINT ["python3"]

CMD ["app.py"]
